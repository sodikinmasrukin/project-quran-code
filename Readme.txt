QuranCode is an open-source Quran project for Quranic numerical research.

QuranCode is written in C# and the source code is freely available at http://qurancode.codeplex.com

Audio files by many reciters are available from:
http://mirror.transact.net.au/sourceforge/z/project/ze/zekr/recit/
Ex1: afasy-40kbps-offline.recit.zip
Ex2: ghamdi-40kbps-offline.recit.zip
Ex3: hudhaifi-32kbps-offline.recit.zip
1. Unzip xxx.recit.zip file anywhere on disk.
2. Copy audhubillah.mp3 and bismillah.mp3 files and the 001-114 folders to the Audio folder


QuranCode is built as a 3-tier architecture with a fully OO object model.

Tier-1: The data access layer uses XML Serialization to deserialize the Quranic meta data and uses FileIO to read/write Quranic verses.

Tier-2: The business logic layer provides services for searching Quranic verses by text/roots, by number of words, letters, value, and by verse similarity.

Tier-3: The user interface layer is written in WinForms .NET 2.0.
A PocketPC tier-3 layer is also available for Windows Mobile 5.0.


The model overview:
----------------------------
Book-->Verse                 [verse objects]
----------------------------
----------------------------
Book-->Chapter-->Verse       [verse pointers]
----------------------------
----------------------------
Book-->Station-->Verse       [verse pointers]
----------------------------
----------------------------
Book-->Part-->Verse          [verse pointers]
----------------------------
----------------------------
Book-->Group-->Verse         [verse pointers]
----------------------------
----------------------------
Book-->GroupQuarter-->Verse  [verse pointers]
----------------------------
----------------------------
Book-->Bowing-->Verse        [verse pointers]
----------------------------
----------------------------
Book-->Page-->Verse          [verse pointers]
----------------------------

where:
  Book = Quran		(1..*) (original and simplified ones)
  Chapter = Sura	(1-114)
  Verse = Aya		(1-6236)
  Station = Menzal	(1-7)
  Part = Juz'		(1-30)
  Group = Hisb		(1-60)
  GroupQuarter = Rubi3	(1-240)
  Bowing = Ruk3a	(1-556)


Please feel free to use the software for any purpose GOD Almighty agrees with.

Be a guiding light!

Ali Adams
God > infinity
www.heliwave.com
